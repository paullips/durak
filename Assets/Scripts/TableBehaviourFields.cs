﻿using System.Collections.Generic;

using UnityEngine;

public partial class TableBehaviour : MonoBehaviour
{
	public Texture i6_of_clubs;
	public Texture i6_of_hearts;
	public Texture i6_of_diamonds;
	public Texture i6_of_spades;
	public Texture i7_of_clubs;
	public Texture i7_of_hearts;
	public Texture i7_of_diamonds;
	public Texture i7_of_spades;
	public Texture i8_of_clubs;
	public Texture i8_of_hearts;
	public Texture i8_of_diamonds;
	public Texture i8_of_spades;
	public Texture i9_of_clubs;
	public Texture i9_of_hearts;
	public Texture i9_of_diamonds;
	public Texture i9_of_spades;
	public Texture i10_of_clubs;
	public Texture i10_of_hearts;
	public Texture i10_of_diamonds;
	public Texture i10_of_spades;
	public Texture jack_of_clubs;
	public Texture jack_of_hearts;
	public Texture jack_of_diamonds;
	public Texture jack_of_spades;
	public Texture queen_of_clubs;
	public Texture queen_of_hearts;
	public Texture queen_of_diamonds;
	public Texture queen_of_spades;
	public Texture king_of_clubs;
	public Texture king_of_hearts;
	public Texture king_of_diamonds;
	public Texture king_of_spades;
	public Texture ace_of_clubs;
	public Texture ace_of_hearts;
	public Texture ace_of_diamonds;
	public Texture ace_of_spades;
	public Texture background;

	private void Start()
	{
		CardsMappins = new Dictionary<string, Texture>
		{
			{"6_of_clubs", i6_of_clubs},
			{"6_of_hearts", i6_of_hearts},
			{"6_of_diamonds", i6_of_diamonds},
			{"6_of_spades", i6_of_spades},
			{"7_of_clubs", i7_of_clubs},
			{"7_of_hearts", i7_of_hearts},
			{"7_of_diamonds", i7_of_diamonds},
			{"7_of_spades", i7_of_spades},
			{"8_of_clubs", i8_of_clubs},
			{"8_of_hearts", i8_of_hearts},
			{"8_of_diamonds", i8_of_diamonds},
			{"8_of_spades", i8_of_spades},
			{"9_of_clubs", i9_of_clubs},
			{"9_of_hearts", i9_of_hearts},
			{"9_of_diamonds", i9_of_diamonds},
			{"9_of_spades", i9_of_spades},
			{"10_of_clubs", i10_of_clubs},
			{"10_of_hearts", i10_of_hearts},
			{"10_of_diamonds", i10_of_diamonds},
			{"10_of_spades", i10_of_spades},
			{"jack_of_clubs", jack_of_clubs},
			{"jack_of_hearts", jack_of_hearts},
			{"jack_of_diamonds", jack_of_diamonds},
			{"jack_of_spades", jack_of_spades},
			{"queen_of_clubs", queen_of_clubs},
			{"queen_of_hearts", queen_of_hearts},
			{"queen_of_diamonds", queen_of_diamonds},
			{"queen_of_spades", queen_of_spades},
			{"king_of_clubs", king_of_clubs},
			{"king_of_hearts", king_of_hearts},
			{"king_of_diamonds", king_of_diamonds},
			{"king_of_spades", king_of_spades},
			{"ace_of_clubs", ace_of_clubs},
			{"ace_of_hearts", ace_of_hearts},
			{"ace_of_diamonds", ace_of_diamonds},
			{"ace_of_spades", ace_of_spades},
			{"background", background},
		};

		_gameSettings = new LoginBox();
		_gameInterface = new GameTableInterface(CardsMappins);
	}
}
