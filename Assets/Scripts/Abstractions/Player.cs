﻿using System;
using System.Collections.Generic;

public abstract class Player
{
	private const int MaximumAllowedNumberOfPlayers = 6;

	public int TurnOrder { get; private set; }
	public string Name { get; private set; }
	public List<Card> Hand { get; private set; }

	protected Player(int priority, string nameEntry, List<Card> startingHand)
	{
		if (nameEntry == null)
			throw new ArgumentNullException("nameEntry");
		if (startingHand == null)
			throw new ArgumentNullException("startingHand");

		TurnOrder = priority;
		Hand = startingHand;
		Name = nameEntry;
	}

	public void PassTurn()
	{
		TurnOrder += MaximumAllowedNumberOfPlayers;
	}

	/// <summary>
	/// Спрашивает у игрока, какими картами он собирается ходить:
	/// </summary>
	public abstract List<Card> GetCardsToPlay(Table gameTable);

	/// <summary>
	/// Спрашивает игрока, как он собирается отбиваться:
	/// </summary>
	public abstract List<Card> TryCounter(Table gameTable);

	protected sealed class CardDto
	{
		public Card Card { get; set; }
		public int PseudoValue { get; set; }

		public static CardDto FromCard(Card realCardEntry, Suits niceSuitCard)
		{
			if (realCardEntry == null)
				throw new ArgumentNullException("realCardEntry");

			var cardEntry = realCardEntry;
			var pseudoValueEntry = (int) realCardEntry.Value + 1;

			if (realCardEntry.Suit.Equals(niceSuitCard))
			{
				pseudoValueEntry = pseudoValueEntry * 1000;  // Просто считаем, что каждый козырь этом очень дорогая карта!
			}

			return new CardDto
			{
				Card = cardEntry,
				PseudoValue = pseudoValueEntry,
			};
		}
	}
}
