﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

public sealed class Table : IDisposable
{
	private const string ComputerPlayerNameMaskEntry = "Игрок {0}";
	private const int RequiredStartingHandSize = 6;

	private Thread _gameTurnThread;

	public int NumberOfPlayers { get; private set; }
	public Deck Deck { get; private set; }
	public List<Player> Players { get; private set; }
	public List<Card> DiscardedCards { get; private set; }
	public Suits TrumpSuit { get; private set; }
	public InteroperationalContainer InteroperationalContainer { get; private set; }

	public Player Human { get; private set; }
	public Player Durak { get; private set; }
	public Player ActivePlayer { get; private set; }
	public Player DefensivePlayer { get; private set; }

	public bool BreakThreads { get; private set; }
	public Dictionary<Player, List<Card>> PlayedCards { get; private set; }

	public int CardsInPlayCount
	{
		get
		{
			return PlayedCards.Sum(p => p.Value.Count);
		}
	}

	public Table(int numberOfPlayers, string mainPlayerNameEntry)
	{
		if (numberOfPlayers < 2 || numberOfPlayers > 6)
			throw new InvalidOperationException(
				"Недопустимое количество игроков!");
		if (mainPlayerNameEntry == null)
			throw new ArgumentNullException("mainPlayerNameEntry");
		if (numberOfPlayers * RequiredStartingHandSize > Deck.MaximumSize)
			throw new InvalidOperationException(
				"Для такого количества игроков и размера колоды нельзя брать стартовую руку такого размера!");

		NumberOfPlayers = numberOfPlayers;

		Deck = new Deck();
		DiscardedCards = new List<Card>();
		Players = new List<Player>();
		PlayedCards = new Dictionary<Player, List<Card>>();
		InteroperationalContainer = new InteroperationalContainer();

		// Эта карта всё равно окажется внизу, так что можно просто взять масть нижней в качестве козыря:
		TrumpSuit = Deck.BottomCard.Suit;

		var playerDtos = new List<PlayerDto>
		{
			new PlayerDto
			{
				IsRealPlayer = true,
				Cards = DrawStartingHand(),
			},
		};

		for (var i = 1; i < numberOfPlayers; i++)
		{
			playerDtos.Add(
				new PlayerDto
				{
					IsRealPlayer = false,
					Cards = DrawStartingHand(),
				});
		}

		// Отсортируем игроков по старшему козырю:
		playerDtos = playerDtos.OrderByDescending(p => p.GetHighestOfSuit(TrumpSuit)).ToList();

		// Строим список игроков:
		Players = playerDtos
			.Select(p =>
			{
				var priority = playerDtos.IndexOf(p) + 1;

				return p.IsRealPlayer
					? Human = new RealPlayer(priority, mainPlayerNameEntry, p.Cards)
					: new ComputerPlayer(priority, GetComputerPlayerName(priority), p.Cards);
			})
			.OrderBy(p => p.TurnOrder)
			.ToList();
	}

	public void DoTurn()
	{
		if (_gameTurnThread == null || !_gameTurnThread.IsAlive)
		{
			// Таким образом мы сможем приостановить ход в ожидании пользовательского ввода:
			_gameTurnThread = new Thread(DoTurnInner);

			_gameTurnThread.Start();
		}
	}

	private void DoTurnInner()
	{
		// Нельзя переходить к следующему ходу, когда игра закончилась:
		if (Durak != null)
		{
			return;
		}

		Player durak;

		if (DefensivePlayer != null && PlayedCards[DefensivePlayer].Count == 0)
		{
			// Взять в руку то, что лежит на столе:
			DefensivePlayer.Hand.AddRange(PlayedCards.SelectMany(c => c.Value));
		}
		else
		{
			// Отправить в брос то, что лежит на столе:
			DiscardedCards.AddRange(PlayedCards.SelectMany(c => c.Value));
		}

		// К этому моменту ход «активного» игрока уже закончился, поэтому нужно явным образом заставить его брать карты первым, а остальных в порядке очереди:
		if (ActivePlayer != null)
		{
			foreach (var player in new[] { ActivePlayer }.Concat(Players.OrderByDescending(p => p.TurnOrder).Where(p => p != ActivePlayer)))
			{
				while (player.Hand.Count < RequiredStartingHandSize && Deck.Count > 0)
				{
					var card = Deck.Cards.Last();
					Deck.Cards.Remove(card);
					player.Hand.Add(card);
				}
			}
		}

		var activePlayerEntry = ActivePlayer = GetActivePlayer();
		var defensivePlayerEntry = DefensivePlayer = GetDefensivePlayer();

		PlayedCards.Clear();

		// Активный игрок ходит:
		PlayedCards.Add(activePlayerEntry, activePlayerEntry.GetCardsToPlay(this));

		// Вдруг активный игрок сбросил карты и остался только один последний:
		if (CheckLoseConditions(out durak))
		{
			Durak = durak;
			return;
		}

		// Вдруг другие игроки сбросили карты и остался последний:
		if (CheckLoseConditions(out durak))
		{
			Durak = durak;
			return;
		}

		var defensiveCards = defensivePlayerEntry.TryCounter(this);

		// Игрок, под которого ходят, пытается отбиться:
		PlayedCards.Add(defensivePlayerEntry, defensiveCards);

		if (defensiveCards.Count == 0)
		{
			defensivePlayerEntry.PassTurn(); // Считаем, что он походил и пропускает следующий ход!
		}

		// Игрок отбился последней картой:
		if (CheckLoseConditions(out durak))
		{
			Durak = durak;
			return;
		}

		ActivePlayer.PassTurn();
	}

	private bool CheckLoseConditions(out Player durak)
	{
		durak = null;

		if (Deck.Count > 0)
		{
			return false;
		}
		else
		{
			var playersWithCards = Players.Where(p => p.Hand.Count > 0).ToArray();

			if (playersWithCards.Length == 1)
			{
				durak = playersWithCards[0];
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	private Player GetActivePlayer()
	{
		// Игроки добирают карты до того, как мы определяем активного, а значит можно отсеивать игроков с пустой рукой:
		return Players.Where(p => p.Hand.Count > 0).OrderBy(p => p.TurnOrder).First();
	}

	private Player GetDefensivePlayer()
	{
		// Игроки добирают карты до того, как мы определяем защищающегося, а значит можно отсеивать игроков с пустой рукой:
		return Players.Where(p => p.Hand.Count > 0).OrderBy(p => p.TurnOrder).Skip(1).First();
	}

	private List<Card> DrawStartingHand()
	{
		var result = new List<Card>();
		
		for (var i = 0; i < RequiredStartingHandSize; i++)
		{
			var card = Deck.Cards.Last();
			Deck.Cards.Remove(card);
			result.Add(card);
		}

		return result;
	}

	private string GetComputerPlayerName(int playerOrderPriority)
	{
		return String.Format(ComputerPlayerNameMaskEntry, playerOrderPriority);
	}

	public void Dispose()
	{
		BreakThreads = true;

		if (_gameTurnThread != null)
		{
			_gameTurnThread.Abort();
		}
	}

	private class PlayerDto
	{
		public bool IsRealPlayer { get; set; }
		public List<Card> Cards { get; set; }

		public int GetHighestOfSuit(Suits suitRequired)
		{
			var card = Cards.Where(c => c.Suit.Equals(suitRequired)).OrderByDescending(c => c.Value).FirstOrDefault();
			return (card != null) ? (int) card.Value : -1;
		}
	}
}
