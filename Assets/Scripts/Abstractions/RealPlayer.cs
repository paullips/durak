﻿using System;
using System.Collections.Generic;
using System.Linq;

public sealed class RealPlayer : Player
{
	public RealPlayer(int priority, string nameEntry, List<Card> startingHand) : base(priority, nameEntry, startingHand)
	{
	}

	public override List<Card> GetCardsToPlay(Table gameTable)
	{
		if (gameTable == null)
			throw new ArgumentNullException("gameTable");

		if (gameTable.Deck.Count == 0 && Hand.Count == 0)
		{
			return Enumerable.Empty<Card>().ToList();
		}

		gameTable.InteroperationalContainer.WaitingForInput = true;

		// Некоторый внешний многопоточный буффер должен предоставить информацию о том, какими картами мы ходим:
		while (true)
		{
			if (gameTable.BreakThreads)
			{
				throw new InvalidOperationException(
					"Поток выполнения подпрограммы был прерван!");
			}

			if (gameTable.InteroperationalContainer.CardSelected)
			{
				if (IsValidCardsSelectedForAttacking(gameTable))
				{
					var cards = gameTable.InteroperationalContainer.SelectedCardsInHand.ToList(); // Нам необходимо получить копию этого списка из другого потока!

					// Убираем эти карты из руки:
					Hand.RemoveAll(c => cards.Contains(c));

					gameTable.InteroperationalContainer.CardSelected = false;
					gameTable.InteroperationalContainer.WaitingForInput = false;
					gameTable.InteroperationalContainer.SelectedCardsInHand.Clear();

					return cards;
				}
				else
				{
					gameTable.InteroperationalContainer.CardSelected = false;
					gameTable.InteroperationalContainer.SelectedCardsInHand.Clear();
				}
			}
		}
	}

	public override List<Card> TryCounter(Table gameTable)
	{
		if (gameTable == null)
			throw new ArgumentNullException("gameTable");

		if (gameTable.Deck.Count == 0 && Hand.Count == 0)
		{
			return Enumerable.Empty<Card>().ToList();
		}

		gameTable.InteroperationalContainer.WaitingForInput = true;

		// Некоторый внешний многопоточный буффер должен предоставить информацию о том, какими картами мы отбиваемся:
		while (true)
		{
			if (gameTable.BreakThreads)
			{
				throw new InvalidOperationException(
					"Поток выполнения подпрограммы был прерван!");
			}

			if (gameTable.InteroperationalContainer.CardSelected)
			{
				if (IsValidCardsSelectedForDefending(gameTable))
				{
					var cards = gameTable.InteroperationalContainer.SelectedCardsInHand.ToList(); // Нам необходимо получить копию этого списка из другого потока!

					// Убираем эти карты из руки:
					Hand.RemoveAll(c => cards.Contains(c));

					gameTable.InteroperationalContainer.CardSelected = false;
					gameTable.InteroperationalContainer.WaitingForInput = false;
					gameTable.InteroperationalContainer.SelectedCardsInHand.Clear();

					return cards;
				}
				else
				{
					gameTable.InteroperationalContainer.CardSelected = false;
					gameTable.InteroperationalContainer.SelectedCardsInHand.Clear();
				}
			}
		}
	}

	private bool IsValidCardsSelectedForAttacking(Table gameTableEntry)
	{
		var maximumCardsInPlay = 6;

		if (TurnOrder == 1)
		{
			maximumCardsInPlay = 5;
		}

		var proposedCards = gameTableEntry.InteroperationalContainer.SelectedCardsInHand.ToList();

		var minimumCards = gameTableEntry.ActivePlayer == this ? 1 : 0;
		var maximumCards = maximumCardsInPlay - gameTableEntry.CardsInPlayCount;

		if (proposedCards.Count < minimumCards || proposedCards.Count > maximumCards)
		{
			return false;
		}

		return 1 == proposedCards.GroupBy(c => c.Value).Count(); // Все карты одной стоимости!
	}

	private bool IsValidCardsSelectedForDefending(Table gameTableEntry)
	{
		var proposedCards = gameTableEntry.InteroperationalContainer.SelectedCardsInHand.ToList();

		// Игрок должен побить все карты или не выкладывать их вообще, чтобы забрать:
		if (proposedCards.Count != 0 && proposedCards.Count != gameTableEntry.CardsInPlayCount)
		{
			return false;
		}
		else if (proposedCards.Count == 0)
		{
			return true;
		}
		else
		{
			var allAttackingCards = gameTableEntry.PlayedCards.SelectMany(c => c.Value).ToList();
			var possibleDefensiveCards = new List<Card>();

			foreach (var card in allAttackingCards)
			{
				var value = (int) card.Value + 1;

				if (card.Suit == gameTableEntry.TrumpSuit)
				{
					value = value * 1000; // Просто считаем, что каждый козырь этом очень дорогая карта!
				}

				// Попытаемся побить все сыгранные против нас карты теми, что выбрал игрок:
				var minimalCard = proposedCards
					.Where(c => !possibleDefensiveCards.Contains(c) && (c.Suit.Equals(card.Suit) || c.Suit.Equals(gameTableEntry.TrumpSuit)))
					.Select(entry => CardDto.FromCard(entry, gameTableEntry.TrumpSuit))
					.Where(c => value < c.PseudoValue)
					.OrderBy(c => c.PseudoValue)
					.FirstOrDefault();

				if (minimalCard == null)
				{
					return false;
				}
				else
				{
					possibleDefensiveCards.Add(minimalCard.Card);
				}
			}

			return true;
		}
	}
}
