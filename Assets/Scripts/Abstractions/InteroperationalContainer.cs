﻿using System.Collections.Generic;

public sealed class InteroperationalContainer
{
	public bool CardSelected { get; set; }
	public bool WaitingForInput { get; set; }
	public List<Card> SelectedCardsInHand { get; set; }

	public InteroperationalContainer()
	{
		SelectedCardsInHand = new List<Card>();
	}
}
