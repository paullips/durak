﻿public sealed class Card
{
	public Suits Suit { get; private set; }
	public Values Value { get; private set; }

	public string Name
	{
		get
		{
			return StringHelpers.GetCardName(this);
		}
	}

	public Card(Suits suitEntry, Values valueEntry)
	{
		Suit = suitEntry;
		Value = valueEntry;
	}
}
