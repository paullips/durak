﻿using System;
using System.Collections.Generic;
using System.Linq;

public sealed class ComputerPlayer : Player
{
	public ComputerPlayer(int priority, string nameEntry, List<Card> startingHand) : base(priority, nameEntry, startingHand)
	{
	}

	public override List<Card> GetCardsToPlay(Table gameTable)
	{
		if (gameTable == null)
			throw new ArgumentNullException("gameTable");

		var maximumCardsInPlay = 6;

		if (TurnOrder == 1)
		{
			maximumCardsInPlay = 5;
		}

		var howManyCardsRequired = maximumCardsInPlay - gameTable.CardsInPlayCount;

		if (howManyCardsRequired == 0 || Hand.Count == 0)
		{
			return Enumerable.Empty<Card>().ToList();
		}

		var random = new Random();
		List<Card> randomCards = new List<Card>();

		// Это мой ход и можно сходить любой картой:
		var randomCard = Hand.OrderBy(c => Guid.NewGuid()).First();
		var additionalCards = Hand.Where(c => c != randomCard && c.Value.Equals(randomCard.Value)).ToArray();

		randomCards.Add(randomCard);

		if (additionalCards.Length != 0)
		{
			randomCards.AddRange(
				additionalCards.Take(
					random.Next(
						0,
						Math.Min(
							additionalCards.Length,
							howManyCardsRequired))));
		}

		// Убираем эти карты из руки:
		Hand.RemoveAll(c => randomCards.Contains(c));
		return randomCards;
	}

	public override List<Card> TryCounter(Table gameTable)
	{
		if (gameTable == null)
			throw new ArgumentNullException("gameTable");

		var allAttackingCards = gameTable.PlayedCards.SelectMany(c => c.Value).ToList();
		var possibleDefensiveCards = new List<Card>();

		foreach (var card in allAttackingCards)
		{
			var value = (int) card.Value + 1;

			if (card.Suit == gameTable.TrumpSuit)
			{
				value = value * 1000; // Просто считаем, что каждый козырь этом очень дорогая карта!
			}

			var minimalCard = Hand
				.Where(c => !possibleDefensiveCards.Contains(c) && (c.Suit.Equals(card.Suit) || c.Suit.Equals(gameTable.TrumpSuit)))
				.Select(entry => CardDto.FromCard(entry, gameTable.TrumpSuit))
				.Where(c => value < c.PseudoValue)
				.OrderBy(c => c.PseudoValue)
				.FirstOrDefault();

			if (minimalCard != null)
			{
				possibleDefensiveCards.Add(minimalCard.Card);
			}
		}

		if (possibleDefensiveCards.Count == allAttackingCards.Count || (possibleDefensiveCards.Count == Hand.Count && gameTable.Deck.Count == 0))
		{
			Hand.RemoveAll(c => possibleDefensiveCards.Contains(c));
			return possibleDefensiveCards;
		}
		else
		{
			return Enumerable.Empty<Card>().ToList();
		}
	}
}
