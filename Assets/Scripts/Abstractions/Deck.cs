﻿using System;
using System.Collections.Generic;
using System.Linq;

public sealed class Deck
{
	public const int MaximumSize = 36;

	public List<Card> Cards { get; private set; }
	
	public int Count
	{
		get
		{
			return Cards.Count;
		}
	}

	public Card BottomCard
	{
		get
		{
			return Cards.Count > 0 ? Cards[0] : null;
		}
	}

	public Deck()
	{
		Cards = new List<Card>(MaximumSize);

		for (var i = 0; i < GetEnumSize<Suits>(); i++)
		{
			for (var j = 0; j < GetEnumSize<Values>(); j++)
			{
				Cards.Add(new Card((Suits) i, (Values) j));
			}
		}

		Cards = Cards.OrderBy(a => Guid.NewGuid()).ToList();
	}

	private int GetEnumSize<TEnum>() where TEnum : struct
	{
		return Enum.GetNames(typeof(TEnum)).Length;
	}
}
