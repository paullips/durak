﻿using System.Collections.Generic;

using UnityEngine;

public partial class TableBehaviour : MonoBehaviour
{
	private Table _tableEntry;
	private LoginBox _gameSettings;
	private GameTableInterface _gameInterface;

	public Dictionary<string, Texture> CardsMappins { get; private set; }

	private void Update()
	{
		// Если нажат пробел, игра находится в правильном состоянии и не закончилась:
		if (_tableEntry != null && _tableEntry.Durak == null && Input.GetKeyDown(KeyCode.Space))
		{
			_tableEntry.DoTurn(); // Начать новый ход!
		}
	}

	private void OnGUI()
	{
		if (_tableEntry != null)
		{
			_gameInterface.Draw(ref _tableEntry);
		}
		else
		{
			_gameSettings.Draw(ref _tableEntry);
		}
	}

	private void OnApplicationQuit()
	{
		if (_tableEntry != null)
		{
			_tableEntry.Dispose();
		}
	}
}
