﻿using System;
using System.Text;

public static class StringHelpers
{
	/// <summary>
	/// Получить полное название карты в виде текста.
	/// </summary>
	public static string GetCardName(Card card)
	{
		if (card == null)
			throw new ArgumentNullException("card");

		var nameBuilder = new StringBuilder();

		nameBuilder.Append(GetValueName(card.Value) + " ");

		switch (card.Suit)
		{
			case Suits.Cups:
				nameBuilder.Append("Пик");
				break;
			case Suits.Coins:
				nameBuilder.Append("Червей");
				break;
			case Suits.Clubs:
				nameBuilder.Append("Бубён");
				break;
			case Suits.Swords:
				nameBuilder.Append("Треф");
				break;
			default:
				throw new NotImplementedException();
		}

		return nameBuilder.ToString();
	}

	/// <summary>
	/// Получить имя текстуры карты в словаре текстур.
	/// </summary>
	public static string GetTextureFileName(Card card)
	{
		if (card == null)
			throw new ArgumentNullException("card");

		var nameBuilder = new StringBuilder();

		switch (card.Value)
		{
			case Values.Six:
				nameBuilder.Append("6");
				break;
			case Values.Seven:
				nameBuilder.Append("7");
				break;
			case Values.Eight:
				nameBuilder.Append("8");
				break;
			case Values.Nine:
				nameBuilder.Append("9");
				break;
			case Values.Ten:
				nameBuilder.Append("10");
				break;
			case Values.Jack:
				nameBuilder.Append("jack");
				break;
			case Values.Queen:
				nameBuilder.Append("queen");
				break;
			case Values.King:
				nameBuilder.Append("king");
				break;
			case Values.Ace:
				nameBuilder.Append("ace");
				break;
			default:
				throw new NotImplementedException();
		}

		nameBuilder.Append("_of_");

		switch (card.Suit)
		{
			case Suits.Cups:
				nameBuilder.Append("spades");
				break;
			case Suits.Coins:
				nameBuilder.Append("hearts");
				break;
			case Suits.Clubs:
				nameBuilder.Append("diamonds");
				break;
			case Suits.Swords:
				nameBuilder.Append("clubs");
				break;
			default:
				throw new NotImplementedException();
		}

		return nameBuilder.ToString();
	}

	/// <summary>
	/// Получить название номинала карты.
	/// </summary>
	public static string GetValueName(Values value)
	{
		switch (value)
		{
			case Values.Six:
				return "Шестёрка";
			case Values.Seven:
				return "Семёрка";
			case Values.Eight:
				return "Восьмёрка";
			case Values.Nine:
				return "Девятка";
			case Values.Ten:
				return "Десятка";
			case Values.Jack:
				return "Валет";
			case Values.Queen:
				return "Дама";
			case Values.King:
				return "Король";
			case Values.Ace:
				return "Туз";
			default:
				throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Получить название масти в множественном числе.
	/// </summary>
	public static string GetSuitName(Suits suit)
	{
		switch (suit)
		{
			case Suits.Cups:
				return "Пики";
			case Suits.Coins:
				return "Червы";
			case Suits.Clubs:
				return "Бубны";
			case Suits.Swords:
				return "Трефы";
			default:
				throw new NotImplementedException();
		}
	}
}
