﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class GameTableInterface
{
	private const string SurrenderButtonLabel = "СДАТЬ ЗАНОВО";

	private readonly Texture2D _redBackground;
	private readonly Texture2D _whiteBackground;
	private readonly Texture2D _blackBackground;
	private readonly Dictionary<string, Texture> _cardMappings;

	private static int PortraitHeight
	{
		get
		{
			return (Screen.height - 40) / 3;
		}
	}

	private static int PortraitWidth
	{
		get
		{
			return (Screen.width - 40) / 6;
		}
	}

	private static int PortraitLeftOffset
	{
		get
		{
			return Screen.width - ((Screen.width - 40) / 6 + 10);
		}
	}

	private static int SurrenderLeftOffset
	{
		get
		{
			return (Screen.width - 150) / 2;
		}
	}

	private static int SurrenderTopOffset
	{
		get
		{
			return Screen.height - 40;
		}
	}

	public GameTableInterface(Dictionary<string, Texture> cardMappings)
	{
		if (cardMappings == null)
			throw new ArgumentNullException("cardMappings");

		_cardMappings = cardMappings;
		_whiteBackground = MakeTexture(5, 5, new Color(1f, 1f, 1f, 1f));
		_blackBackground = MakeTexture(5, 5, new Color(0f, 0f, 0f, 1f));
		_redBackground = MakeTexture(5, 5, new Color(1f, 0f, 0f, 1f));
	}

	public void Draw(ref Table gameBoard)
	{
		// Отрисовываем портретики игроков и сыгранные ими карты:
		for (var i = 0; i < gameBoard.NumberOfPlayers; i++)
		{
			var playerEntry = gameBoard.Players[i];

			if (i <= 2)
			{
				DrawPlayerInfoOnLeft(playerEntry, i, gameBoard);
			}
			else
			{
				DrawPlayerInfoOnRight(playerEntry, i - 3, gameBoard);
			}
		}

		// Отрисовываем информационную панельку вверху экрана:
		DrawInformationPanel(ref gameBoard);

		// Отрисовываем панельку с картами в руке игрок:
		DrawHand(ref gameBoard);

		// Отрисовыаем кнопку для сдачи карт заново:
		DrawSurrenderButton(ref gameBoard);
	}

	private void DrawSurrenderButton(ref Table gameBoard)
	{
		var gameSurrendered = GUI.Button(
			new Rect(
				SurrenderLeftOffset,
				SurrenderTopOffset,
				150,
				30),
			SurrenderButtonLabel);

		if (gameSurrendered)
		{
			gameBoard.Dispose();

			gameBoard = null;
		}
	}

	private void DrawInformationPanel(ref Table gameBoard)
	{
		var infoWidth = Screen.width / 6 * 4 - 20;

		GUI.Box(
			new Rect(
				(Screen.width - infoWidth) / 2,
				10,
				infoWidth,
				25),
			String.Format(
				"В КОЛОДЕ: {0} | НИЖНЯЯ: {1} | ОТБОЙ: {2} | КОЗЫРЬ: {3}",
				gameBoard.Deck.Count,
				gameBoard.Deck.BottomCard != null ? StringHelpers.GetCardName(gameBoard.Deck.BottomCard).ToUpper() : "НЕТ",
				gameBoard.DiscardedCards.Count,
				StringHelpers.GetSuitName(gameBoard.TrumpSuit).ToUpper()));
	}

	private void DrawHand(ref Table gameBoard)
	{
		var wideInformationWidthEntry = Screen.width / 6 * 4 - 20;

		var width = (PortraitWidth - 30) / 3;
		var height = (PortraitHeight - 30) / 2;

		GUI.BeginGroup(
			new Rect(((Screen.width - wideInformationWidthEntry) / 2), (Screen.height - (height + 70)), wideInformationWidthEntry, (height + 30)),
			gameBoard.InteroperationalContainer.WaitingForInput ? "[ВЫБЕРИ КАРТЫ]" : String.Empty);

		var whiteStyle = new GUIStyle(GUI.skin.box);
		whiteStyle.normal.background = _whiteBackground;

		var blackStyle = new GUIStyle(GUI.skin.box);
		blackStyle.normal.background = _blackBackground;

		var redStyle = new GUIStyle(GUI.skin.box);
		redStyle.normal.background = _redBackground;

		var index = 0;

		try
		{
			foreach (var playerCard in gameBoard.Human.Hand)
			{
				var texture = _cardMappings[StringHelpers.GetTextureFileName(playerCard)];
				var isCardSelected = gameBoard.InteroperationalContainer.SelectedCardsInHand.Contains(playerCard);

				var backgroundRectangleEntry = new Rect((9 + (width + 5) * index), 20, (width + 2), (height + 2));
				var foregroundRectangleEntry = new Rect((10 + (width + 5) * index), 21, width, height);

				GUI.Box(backgroundRectangleEntry, String.Empty, blackStyle);
				GUI.Box(foregroundRectangleEntry, String.Empty, isCardSelected ? redStyle : whiteStyle);

				if (GUI.Button(foregroundRectangleEntry, texture))
				{
					if (gameBoard.InteroperationalContainer.WaitingForInput)
					{
						if (isCardSelected)
						{
							gameBoard.InteroperationalContainer.SelectedCardsInHand.Remove(playerCard);
						}
						else
						{
							gameBoard.InteroperationalContainer.SelectedCardsInHand.Add(playerCard);
						}
					}
				}

				index++;
			}
		}
		catch (InvalidOperationException e)
		{
			if (!e.Message.Contains("Collection was modified;")) // Ошибка возможна из-за многопоточности, это не страшно, поскольку через мгновение кадр всё равно перерисуется!
			{
				throw e;
			}
		}

		if (gameBoard.InteroperationalContainer.WaitingForInput)
		{
			var backgroundRectangleEntry = new Rect((9 + (width + 5) * index), 30, (width / 2 + 2), (height - 18));
			var foregroundRectangleEntry = new Rect((10 + (width + 5) * index), 31, (width / 2), (height - 20));

			GUI.Box(backgroundRectangleEntry, String.Empty, blackStyle);
			GUI.Box(foregroundRectangleEntry, String.Empty, whiteStyle);

			if (GUI.Button(foregroundRectangleEntry, String.Empty))
			{
				gameBoard.InteroperationalContainer.CardSelected = true;
			}
		}

		GUI.EndGroup();
	}

	private void DrawPlayerInfoOnLeft(Player playerEntry, int screenHeightIndex, Table gameBoard)
	{
		GUI.BeginGroup(
			new Rect(
				10,
				10 + (PortraitHeight + 10) * screenHeightIndex,
				PortraitWidth,
				PortraitHeight));

		DrawPlayerInfo(playerEntry, gameBoard);
		GUI.EndGroup();
	}

	private void DrawPlayerInfoOnRight(Player playerEntry, int screenHeightIndex, Table gameBoard)
	{
		GUI.BeginGroup(
			new Rect(
				PortraitLeftOffset,
				10 + (PortraitHeight + 10) * screenHeightIndex,
				PortraitWidth,
				PortraitHeight));

		DrawPlayerInfo(playerEntry, gameBoard);
		GUI.EndGroup();
	}

	private void DrawPlayerInfo(Player playerEntry, Table gameBoard)
	{
		GUI.Box(
			new Rect(
				0,
				0,
				PortraitWidth,
				PortraitHeight),
			new GUIContent(playerEntry.Name + " (" + playerEntry.Hand.Count + ")"));

		var info = new StringBuilder();

		List<Card> playerCards;

		// Игра не началась:
		if (gameBoard.ActivePlayer == null || gameBoard.DefensivePlayer == null)
		{
			return;
		}

		if (playerEntry == gameBoard.Durak)
		{
			info.AppendLine("[ДУРАК]");
		}
		else if (playerEntry == gameBoard.ActivePlayer)
		{
			info.AppendLine("[ХОДИТ]");
		}
		else if (playerEntry == gameBoard.DefensivePlayer)
		{
			info.AppendLine("[ОТБИВАЕТСЯ]");
		}

		if (gameBoard.PlayedCards.TryGetValue(playerEntry, out playerCards))
		{
			var index = 0;

			for (var x = 0; x < 2; x++)
			{
				for (var y = 0; y < 3; y++)
				{
					if (index >= playerCards.Count)
					{
						continue;
					}

					var texture = _cardMappings[StringHelpers.GetTextureFileName(playerCards[index])];

					var width = (PortraitWidth - 30) / 3;
					var height = (int) ((PortraitHeight - 30) / 3.5);
					var outerRect = new Rect(
							9 + (width + 5) * x,
							39 + (height + 5) * y,
							width + 2,
							height + 2);
					var rect = new Rect(
							10 + (width + 5) * x,
							40 + (height + 5) * y,
							width,
							height);

					var style = new GUIStyle(GUI.skin.box);
					style.normal.background = _whiteBackground;

					var blackStyle = new GUIStyle(GUI.skin.box);
					blackStyle.normal.background = _blackBackground;

					GUI.Box(
						outerRect,
						String.Empty,
						blackStyle);
					GUI.Box(
						rect,
						String.Empty,
						style);
					GUI.DrawTexture(
						rect,
						texture);

					index++;
				}
			}
		}

		GUI.Label(
			new Rect(
				5,
				15,
				PortraitWidth,
				100),
			info.ToString());
	}

	private Texture2D MakeTexture(int width, int height, Color color)
	{
		Color[] pix = new Color[width * height];

		for (int i = 0; i < pix.Length; i++)
		{
			pix[i] = color;
		}

		Texture2D result = new Texture2D(width, height);

		result.SetPixels(pix);
		result.Apply();

		return result;
	}
}
