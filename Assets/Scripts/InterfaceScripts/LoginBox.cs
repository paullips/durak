﻿using System;

using UnityEngine;

public sealed class LoginBox
{
	private const string PlayButtonLabel = "ИГРАТЬ";

	private string _userNameEntry;
	private string _numberOfPlayersEntry;

	#region Position Properties

	private static int LoginWidth
	{
		get
		{
			return Screen.width / 4;
		}
	}

	private static int LoginHeight
	{
		get
		{
			return Screen.height / 4;
		}
	}

	private static int LoginLeftOffset
	{
		get
		{
			return (Screen.width - (Screen.width / 4)) / 2;
		}
	}

	private static int LoginTopOffset
	{
		get
		{
			return (Screen.height - (Screen.height / 4)) / 2;
		}
	}

	#endregion

	public LoginBox()
	{
		// Стандартные значения, чтобы не заполнять их каждый раз при запуске игры:

		_userNameEntry = "Zedrahead";
		_numberOfPlayersEntry = "4";
	}

	public void Draw(ref Table gameBoardEntry)
	{
		GUI.BeginGroup(
			new Rect(
				LoginLeftOffset,
				LoginTopOffset,
				LoginWidth,
				LoginHeight));

		GUI.Box(
			new Rect(
				0,
				0,
				LoginWidth,
				LoginHeight),
			String.Empty);

		var textFieldStyle = GUI.skin.GetStyle("TextField");
		textFieldStyle.alignment = TextAnchor.MiddleCenter;

		_userNameEntry = GUI.TextField(
			new Rect(((LoginWidth - (LoginWidth / 2)) / 2), 10, (LoginWidth / 2), (LoginHeight / 4)),
			_userNameEntry,
			textFieldStyle);

		_numberOfPlayersEntry = GUI.TextField(
			new Rect(((LoginWidth - (LoginWidth / 2)) / 2), (LoginHeight - (LoginHeight / 4)) / 2, (LoginWidth / 2), (LoginHeight / 4)),
			_numberOfPlayersEntry,
			textFieldStyle);

		var buttonPressed = GUI.Button(
			new Rect(((LoginWidth - (LoginWidth / 2)) / 2), LoginHeight - (LoginHeight / 4 + 10), (LoginWidth / 2), LoginHeight / 4),
			PlayButtonLabel);

		GUI.EndGroup();

		if (buttonPressed)
		{
			int numberOfPlayersParsedEntry;

			if (!String.IsNullOrEmpty(_userNameEntry) && Int32.TryParse(_numberOfPlayersEntry, out numberOfPlayersParsedEntry))
			{
				if (numberOfPlayersParsedEntry > 1 && numberOfPlayersParsedEntry <= 6)
				{
					gameBoardEntry = new Table(
						numberOfPlayersParsedEntry,
						_userNameEntry);
				}
			}
		}
	}
}
